// Persistence
// Handles global persistency operations.

export abstract class Persistence {
  public static readonly STORAGE_NAME: string = "Verse2";
  public static readonly DATA_NAME: string = "verse2data";

  public static hasPreviousScene(): boolean {
    return window.localStorage.getItem(this.STORAGE_NAME) !== null;
  }
  public static loadData(): any {
    return window.localStorage.getItem(this.DATA_NAME);
  }
  public static storeData(data: any): void {
    window.localStorage.clear();
    window.localStorage.setItem(this.STORAGE_NAME, "true");
    window.localStorage.setItem(this.DATA_NAME, data);
  }
}
