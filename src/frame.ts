// Frame
// Base class for UI and Composer frames
// Handles any shared functionality.

import { AABB } from "./aabb";
import { IAppCanvas } from "./app";
import * as events from "./events";

export class Frame {
  public onMouseOver: (mouseX: number, mouseY: number) => void;
  public onMouseClick: (mouseX: number, mouseY: number) => void;
  public onMouseDragBegin: (mouseX: number, mouseY: number) => void;
  public onMouseDragUpdate: (mouseX: number, mouseY: number) => void;
  public onMouseDragEnd: (mouseX: number, mouseY: number) => void;
  public onMouseLeave: () => void;

  protected appEvents: events.AppEvents;
  protected appCanvas: IAppCanvas;
  protected startx: number;
  protected width: number;
  protected height: number;
  protected bgColor: string;
  protected aabb: AABB;

  constructor(appEvents: events.AppEvents, appCanvas: IAppCanvas, startx: number, width: number) {
    this.appEvents = appEvents;
    this.appCanvas = appCanvas;
    this.startx = startx;
    this.width = width;
    this.height = this.appCanvas.canvas.height;
    this.bgColor = "#000";
    this.aabb = new AABB(startx, startx + width, 0, this.height);
  }

  public draw(): void {
    this.appCanvas.context.beginPath();
    this.appCanvas.context.fillStyle = this.bgColor;
    this.appCanvas.context.fillRect(this.startx, 0, this.width, this.height);
  }

  public onEvent(event: events.IEvent): void {
    // For receiving events
  }

  // Accessors

  public get boundingBox(): AABB {
    return this.aabb;
  }
}
