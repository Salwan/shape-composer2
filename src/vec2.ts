// Vec2
// Vector 2 container and math. Math methods are non-mutating.

export class Vec2 {
  public x: number;
  public y: number;

  public constructor(x: number = 0.0, y: number = 0.0) {
    this.x = x;
    this.y = y;
  }

  // Returns:  vector + b vector
  public add(b: Vec2): Vec2 {
    return new Vec2(this.x + b.x, this.y + b.y);
  }

  // Returns: vector - b vector
  public sub(b: Vec2): Vec2 {
    return new Vec2(this.x - b.x, this.y - b.y);
  }

  // Returns: direction of this vector in radians
  public direction(): number {
    if (this.x !== 0.0) {
      return Math.atan2(this.y, this.x);
    } else if (this.y !== 0.0) {
      return this.y > 0.0 ? 90.0 * Math.PI / 180.0 : -90.0 * Math.PI / 180.0;
    } else {
      // considered domain error
      // returning zero following standard IEC-60559/IEEE 754
      return 0.0;
    }
  }

  // Returns: magnitude of vec2
  public distance(): number {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }

  // Returns: calculated normal for vector
  public normal(): Vec2 {
    const m: number = this.distance();
    return new Vec2(this.x / m, this.y / m);
  }

  // Returns: rotated vector around a given point (or origin if null).
  public rotate(radians: number, aroundPoint: Vec2 = null): Vec2 {
    const p: Vec2 = aroundPoint != null ? aroundPoint : new Vec2(0, 0);
    const rv: Vec2 = new Vec2(this.x, this.y);
    const x2 = p.x + (rv.x - p.x) * Math.cos(radians) - (rv.y - p.y) * Math.sin(radians);
    const y2 = p.y + (rv.x - p.x) * Math.sin(radians) + (rv.y - p.y) * Math.cos(radians);
    rv.x = x2;
    rv.y = y2;
    return rv;
  }

  public scalexy(sx: number, sy: number): Vec2 {
    return new Vec2(this.x * sx, this.y * sy);
  }
}
