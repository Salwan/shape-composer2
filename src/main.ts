// Shape Composer 2
// App entry script

import { App } from "./app";

let app: App = null;

function viewLoop() {
  requestAnimationFrame(viewLoop);
  app.onFrame();
}

window.onload = () => {
  const elt = document.getElementById("loadingMessage");
  elt.innerText = "";
  const canvas = document.getElementById("scCanvas") as HTMLCanvasElement;
  const context = canvas.getContext("2d");
  // fix for double-click to select
  canvas.onselectstart = () => {
    return false;
  };
  app = new App(canvas, context);
  app.start();
  viewLoop();
};
