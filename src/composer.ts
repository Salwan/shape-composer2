// ComposerFrame
// ShapeComposer window and functionality.

import * as assert from "assert";
import { AABB } from "./aabb";
import { IAppCanvas } from "./app";
import { PALETTE } from "./app";
import { Collision } from "./collision";
import * as events from "./events";
import { Frame } from "./frame";
import { Persistence } from "./persistence";
import { SelectionBox } from "./selection-box";
import { EShape, IShape, Shape } from "./shape";
import { ShapeTools } from "./shape-tools";
import { Vec2 } from "./vec2";

export class ComposerFrame extends Frame {
  private shapes: Shape[];
  private mouseOverShape: Shape;
  private selectionBox: SelectionBox;

  constructor(appEvents: events.AppEvents, appCanvas: IAppCanvas, startx: number, width: number) {
    super(appEvents, appCanvas, startx, width);
    this.bgColor = "#ddd";
    this.loadScene();
  }

  // Draw everything

  public draw(): void {
    super.draw();
    this.appCanvas.context.lineWidth = 0.5;
    this.appCanvas.context.font = "13px arial";
    for (const shape of this.shapes) {
      shape.draw(this.appCanvas, 0.5);
    }
    if (this.selectionBox) {
      this.selectionBox.draw(this.appCanvas);
    }
  }

  // Events

  public onEvent(event: events.IEvent): void {
    if (event.type === events.Events.CREATE) {
      const d: number = Math.floor(Math.random() * 128 + 128);
      this.shapes.push(
        this.translateShape(
          this.createShape(event.data as EShape, d, d, this.randomColor()),
          64 + Math.random() * (this.width - 128),
          64 + Math.random() * (this.height - 128)
        )
      );
      this.appEvents.broadcastEvent(events.Events.STORESCENE);
    } else if (event.type === events.Events.DELETE) {
      if (this.selectionBox) {
        const shape: Shape = this.selectionBox.selectedShape;
        this.unselect();
        const idx: number = this.shapes.indexOf(shape);
        if (idx > -1) {
          this.shapes.splice(idx, 1);
        }
      }
      this.appEvents.broadcastEvent(events.Events.STORESCENE);
    } else if (event.type === events.Events.RESETSCENE) {
      this.unselect();
      this.resetScene();
    } else if (event.type === events.Events.STORESCENE) {
      this.storeScene();
    }
  }

  // Mouse interactions

  public onMouseOver = (mouseX: number, mouseY: number): void => {
    // Reset mouse over highlight
    if (this.mouseOverShape) {
      this.mouseOverShape.mouseOver = false;
    }
    // Changing mouse cursor if over selectionBox
    if (this.selectionBox) {
      if (this.selectionBox.isInsideTransform(mouseX, mouseY)) {
        this.selectionBox.onMouseOver(this.appCanvas, mouseX, mouseY);
      } else {
        this.selectionBox.resetMouseCursor(this.appCanvas);
      }
    }
    // Check pixel at mouse position from collision canvas for shape id
    const shape = this.getShapeCollision(mouseX, mouseY);
    if (shape) {
      shape.mouseOver = true;
      this.mouseOverShape = shape;
    } else {
      this.mouseOverShape = null;
    }
  };

  public onMouseLeave = (): void => {
    if (this.mouseOverShape) {
      this.mouseOverShape.mouseOver = false;
      this.mouseOverShape = null;
    }
    if (this.selectionBox) {
      this.selectionBox.resetMouseCursor(this.appCanvas);
    }
  };

  public onMouseClick = (mouseX: number, mouseY: number): void => {
    const shape = this.getShapeCollision(mouseX, mouseY);
    if (shape) {
      this.selectionBox = new SelectionBox(shape, this.appEvents);
    } else {
      this.unselect();
    }
  };

  public onMouseDragBegin = (mouseX: number, mouseY: number): void => {
    let dragTaken: boolean = false;
    if (this.selectionBox) {
      dragTaken = this.selectionBox.onMouseDragBegin(mouseX, mouseY);
    }
    // If dragging on non-selected shape, select and drag that shape!
    if (!dragTaken) {
      const shape = this.getShapeCollision(mouseX, mouseY);
      if (shape) {
        this.unselect(true);
        this.selectionBox = new SelectionBox(shape, this.appEvents);
        this.selectionBox.onMouseDragBegin(mouseX, mouseY);
      }
    }
  };

  public onMouseDragUpdate = (mouseX: number, mouseY: number): void => {
    if (this.selectionBox) {
      this.selectionBox.onMouseDragUpdate(mouseX, mouseY);
    }
  };

  public onMouseDragEnd = (mouseX: number, mouseY: number): void => {
    if (this.selectionBox) {
      this.selectionBox.onMouseDragEnd(mouseX, mouseY);
    }
  };

  // Privates

  private resetScene(): void {
    // Create starter shapes
    this.shapes = [];
    this.shapes.push(
      this.translateShape(
        this.createShape(EShape.rect, 128, 128, this.randomColor()),
        this.width / 2 - 128,
        this.height / 2 - 92
      )
    );
    this.shapes.push(
      this.translateShape(
        this.createShape(EShape.circle, 128, 128, this.randomColor()),
        this.width / 2 + 128,
        this.height / 2 - 92
      )
    );
    this.shapes.push(
      this.translateShape(
        this.createShape(EShape.triangle, 128, 128, this.randomColor()),
        this.width / 2 - 128,
        this.height / 2 + 92
      )
    );
    this.shapes.push(
      this.translateShape(
        this.createShape(EShape.star, 128, 128, this.randomColor()),
        this.width / 2 + 128,
        this.height / 2 + 92
      )
    );
    this.storeScene();
  }

  private storeScene(): void {
    const scene: IShape[] = [];
    for (const shape of this.shapes) {
      scene.push(shape.data);
    }
    Persistence.storeData(JSON.stringify(scene));
  }

  private loadScene(): void {
    if (Persistence.hasPreviousScene()) {
      this.shapes = [];
      const save: IShape[] = JSON.parse(Persistence.loadData()) as IShape[];
      for (const shape of save) {
        const verts: Vec2[] = [];
        for (const v of shape.verts) {
          verts.push(new Vec2(v.x, v.y));
        }
        this.shapes.push(new Shape(verts, shape.color));
      }
    } else {
      this.resetScene();
    }
  }

  private unselect(dontSendEvent: boolean = false): void {
    if (this.selectionBox) {
      this.selectionBox.onUnselect(dontSendEvent);
      this.selectionBox = null;
    }
  }

  private randomColor(): string {
    return PALETTE[Math.floor(Math.random() * PALETTE.length)];
  }

  // Gets shape by Id, returns null if no shape matches Id
  private getShape(id: number): Shape {
    for (const shape of this.shapes) {
      if (shape.id === id) {
        return shape;
      }
    }
    return null;
  }

  // Returns: shape under given x, y. null if nothing is there.
  private getShapeCollision(x: number, y: number): Shape {
    const collisionId = Collision.RGBToId(this.appCanvas.collContext.getImageData(x, y, 1, 1).data);
    return collisionId > 0 ? this.getShape(collisionId) : null;
  }

  private createShape(type: EShape, width: number, height: number, color: string): Shape {
    let verts: Vec2[] = [];
    switch (type) {
      case EShape.rect:
        verts = ShapeTools.Rect(width, height);
        break;
      case EShape.circle:
        verts = ShapeTools.Circle(width / 2);
        break;
      case EShape.triangle:
        verts = ShapeTools.Triangle(width, height);
        break;
      case EShape.star:
        verts = ShapeTools.Star(width / 2);
        break;
    }
    return new Shape(verts, color);
  }

  private translateShape(shape: Shape, dx: number, dy: number): Shape {
    shape.translate(dx, dy);
    return shape;
  }
}
