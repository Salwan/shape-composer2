// Shape
// Each instance represents a shape in-game.

import { AABB } from "./aabb";
import { IAppCanvas } from "./app";
import { Collision } from "./collision";
import { ShapeTools } from "./shape-tools";
import { Vec2 } from "./vec2";

const CIRCLE_SEGMENTS: number = 40;

export enum EShape {
  rect,
  circle,
  triangle,
  star
}

export interface IShape {
  id: number;
  verts: Vec2[];
  color: string;
}

export class Shape {
  private static currentId: number = 0;

  public mouseOver: boolean;

  private shape: IShape;
  private aabb: AABB;

  constructor(vertices: Vec2[], color: string) {
    this.shape = {} as IShape;
    this.shape.id = ++Shape.currentId;
    this.shape.verts = vertices;
    this.shape.color = color;
    this.aabb = this.calculateBoundingBox();
  }

  // Shape drawing
  public draw(appCanvas: IAppCanvas, defaultLineWidth: number): void {
    if (this.shape.verts.length === 0) {
      throw new Error(`Shape ${this.shape.id}: has an empty vertex list.`);
    }
    appCanvas.context.beginPath();
    appCanvas.context.fillStyle = this.shape.color;
    appCanvas.context.strokeStyle = this.mouseOver ? "#fff" : "#000";
    const path: Path2D = new Path2D();
    path.moveTo(
      this.shape.verts[this.shape.verts.length - 1].x,
      this.shape.verts[this.shape.verts.length - 1].y
    );
    for (const v of this.shape.verts) {
      path.lineTo(v.x, v.y);
    }
    appCanvas.context.fill(path);
    appCanvas.context.lineWidth = this.mouseOver ? 3 : defaultLineWidth;
    appCanvas.context.stroke(path);

    // Shape collision
    appCanvas.collContext.fillStyle = Collision.IdToColor(this.shape.id);
    appCanvas.collContext.fill(path);

    appCanvas.context.lineWidth = defaultLineWidth;
  }

  public translate(dx: number, dy: number): void {
    this.shape.verts = ShapeTools.Translate(this.shape.verts, [dx, dy]);
    this.aabb = this.calculateBoundingBox();
  }

  public rotate(angleRadians: number): void {
    this.shape.verts = ShapeTools.Rotate(this.shape.verts, this.aabb.center, angleRadians);
    this.aabb = this.calculateBoundingBox();
  }

  public scale(sx: number, sy: number): void {
    this.shape.verts = ShapeTools.Scale(this.shape.verts, this.aabb.center, [sx, sy]);
    this.aabb = this.calculateBoundingBox();
  }

  // Accessors
  public get data(): IShape {
    return this.shape;
  }

  public get id(): number {
    return this.shape.id;
  }

  public get boundingBox(): AABB {
    return this.aabb;
  }

  public get center(): Vec2 {
    return new Vec2(this.aabb.center[0], this.aabb.center[1]);
  }

  // Shape transformation
  private calculateBoundingBox(): AABB {
    const aabb: AABB = new AABB();
    for (const v of this.shape.verts) {
      aabb.minX = v.x < aabb.minX ? v.x : aabb.minX;
      aabb.maxX = v.x > aabb.maxX ? v.x : aabb.maxX;
      aabb.minY = v.y < aabb.minY ? v.y : aabb.minY;
      aabb.maxY = v.y > aabb.maxY ? v.y : aabb.maxY;
    }
    return aabb;
  }
}
