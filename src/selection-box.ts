// Selection Box
// Handles selection box representation and operations.

import { AABB } from "./aabb";
import { IAppCanvas } from "./app";
import { CanvasTools } from "./canvas-tools";
import * as events from "./events";
import { MouseCursor } from "./mouse-cursor";
import { Rect } from "./rect";
import { Shape } from "./shape";
import { Vec2 } from "./vec2";

const SCALER_THICKNESS: number = 10;
const ROTATOR_DISTANCE: number = 20;
const ROTATOR_RADIUS: number = 7;

enum ETransformMode {
  None,
  Moving,
  Rotating,
  Scaling
}

// Selection box broadcasts two events:
// 1. Event indicating something was selected (dispatched on construction)
// 2. Event indicating nothing is selected (called before destruction: onUnselect)
export class SelectionBox {
  private shape: Shape;
  private appEvents: events.AppEvents;
  private aabb: AABB; // AABB of transform box
  private aabbRotator: AABB; // AABB of rotation circle
  private transformMode: ETransformMode;
  private lastTransformPos: Vec2;
  private scalerMultiplier: number;

  constructor(shape: Shape, appEvents: events.AppEvents) {
    this.shape = shape;
    this.appEvents = appEvents;
    this.appEvents.broadcastEvent(events.Events.SELECTION);
    this.transformMode = ETransformMode.None;
    this.lastTransformPos = new Vec2(0, 0);
    this.scalerMultiplier = 1.0;

    this.updateAABBs(this.shape);
  }

  public draw(appCanvas: IAppCanvas): void {
    // Selection box is made up of:
    // - 3 rectangles to indicate bounds
    // - 4 corner circles to show corner scaling areas
    // - 1 circle at the top toe show rotation
    const corners = [[-1, -1], [1, -1], [1, 1], [-1, 1]];
    const context = appCanvas.context;
    const aabb = this.shape.boundingBox;

    // Bounding boxes
    let rc = new Rect(aabb.minX + 1, aabb.minY + 1, aabb.width - 2, aabb.height - 2);
    CanvasTools.StrokeRect(context, rc, "#0f0f0f", 2.0);
    rc = new Rect(aabb.minX + 3, aabb.minY + 3, aabb.width - 6, aabb.height - 6);
    CanvasTools.StrokeRect(context, rc, "#fff", 3.0);

    // Scaler circles
    context.strokeStyle = "#222";
    context.lineWidth = 2.0;
    context.fillStyle = "#efe";
    for (const c of corners) {
      CanvasTools.DrawCircle(
        context,
        aabb.center[0] + c[0] * aabb.width / 2 + -c[0] * 4,
        aabb.center[1] + c[1] * aabb.height / 2 - c[1] * 4,
        4,
        "#efe",
        "#222",
        2.0
      );
    }

    // Dashed border line
    context.setLineDash([2, 2]);
    rc = new Rect(aabb.minX, aabb.minY, aabb.width, aabb.height);
    CanvasTools.StrokeRect(context, rc, "#fff", 1.0);

    // Boundary to rotation circle dashed line
    context.setLineDash([2, 2]);
    CanvasTools.DrawLine(context, "#fff", [aabb.center[0], aabb.minY], [0, -ROTATOR_DISTANCE]);
    context.setLineDash([]);

    // Rotation circle
    CanvasTools.DrawCircle(context, aabb.center[0], aabb.minY - 20, ROTATOR_RADIUS, "#fff", "#222");

    this.updateAABBs(this.shape);
  }

  public isInsideTransform(mouseX: number, mouseY: number): boolean {
    return (
      this.aabb.isPointInside(mouseX, mouseY) || this.aabbRotator.isPointInside(mouseX, mouseY)
    );
  }

  public onUnselect(dontSendEvent: boolean = false): void {
    if (!dontSendEvent) {
      this.appEvents.broadcastEvent(events.Events.NOSELECTION);
    }
    // If unselected while transforming, store scene event
    if (this.transformMode !== ETransformMode.None) {
      this.appEvents.broadcastEvent(events.Events.STORESCENE);
    }
  }

  public onMouseOver(appCanvas: IAppCanvas, mouseX: number, mouseY: number): void {
    const transformMode = this.determineTransformMode(mouseX, mouseY);
    MouseCursor.SetMouseCursorTransform(appCanvas, transformMode);
  }

  // Returns: true if drag is on selection box, false if not
  public onMouseDragBegin(mouseX: number, mouseY: number): boolean {
    const transformType = this.determineTransformMode(mouseX, mouseY);
    this.lastTransformPos = new Vec2(mouseX, mouseY);
    if (transformType === 10) {
      // Rotation circle
      this.transformMode = ETransformMode.Rotating;
    } else if (transformType === 5) {
      // Moving (central area)
      this.transformMode = ETransformMode.Moving;
    } else if (transformType > 0) {
      // Scalers
      // for scaling to match selection box: property scalerMultipler is set
      // by determineTransformMode to 1 for edges, and sqrt(2) for corners
      this.transformMode = ETransformMode.Scaling;
    } else {
      this.transformMode = ETransformMode.None;
      return false;
    }
    return true;
  }

  public onMouseDragUpdate(mouseX: number, mouseY: number): void {
    if (this.transformMode !== ETransformMode.None) {
      const currPos: Vec2 = new Vec2(mouseX, mouseY);
      // Transforming shape
      if (this.transformMode === ETransformMode.Moving) {
        // ---- Translation
        this.shape.translate(
          currPos.x - this.lastTransformPos.x,
          currPos.y - this.lastTransformPos.y
        );
      } else if (this.transformMode === ETransformMode.Rotating) {
        // ---- Rotation
        const rotateFactor: number = (currPos.x - this.lastTransformPos.x) * 0.5 * Math.PI / 180;
        this.shape.rotate(rotateFactor);
      } else if (this.transformMode === ETransformMode.Scaling) {
        // ---- Scaling
        const scaleFactor: number =
          currPos.sub(this.shape.center).distance() /
          (this.shape.boundingBox.width * this.scalerMultiplier / 2);
        this.shape.scale(scaleFactor, scaleFactor);
      }
      this.lastTransformPos.x = mouseX;
      this.lastTransformPos.y = mouseY;
    }
  }

  public onMouseDragEnd(mouseX: number, mouseY: number): void {
    // Store scene if transformation took place
    if (this.transformMode !== ETransformMode.None) {
      this.appEvents.broadcastEvent(events.Events.STORESCENE);
      this.transformMode = ETransformMode.None;
    }
  }

  public resetMouseCursor(appCanvas: IAppCanvas): void {
    MouseCursor.ResetMouseCursor(appCanvas);
  }

  // Accessors

  public get selectedShape(): Shape {
    return this.shape;
  }

  // Privates

  // Updates transform and rotator bounding boxes
  private updateAABBs(shape: Shape): void {
    this.aabb = this.shape.boundingBox;
    // Calculate rotator AABB based on transform AABB
    const left: number = this.aabb.center[0] - ROTATOR_RADIUS;
    const top: number = this.aabb.minY - ROTATOR_DISTANCE - ROTATOR_RADIUS;
    this.aabbRotator = AABB.FromRect(left, top, ROTATOR_RADIUS * 2, ROTATOR_RADIUS * 2);
  }

  // 0: no transform
  // 1, 2, 3, 4, 6, 7, 8, 9: scalers 1=N 2=NE 3=E 4=SE 6=S 7=SW 8=W 9=NW
  // 5: mover
  // 10: rotation
  private determineTransformMode(mouseX: number, mouseY: number): number {
    if (this.aabb.isPointInside(mouseX, mouseY)) {
      let mx: number = 2;
      let my: number = 2;
      // Horizontal
      if (mouseX <= this.aabb.minX + SCALER_THICKNESS) {
        // -- Left side scaler
        mx = 0;
      } else if (mouseX <= this.aabb.maxX - SCALER_THICKNESS) {
        // -- Center area
        mx = 1;
      } // Else: Right side scaler
      // Vertical
      if (mouseY <= this.aabb.minY + SCALER_THICKNESS) {
        // -- Top side scaler
        my = 0;
      } else if (mouseY <= this.aabb.maxY - SCALER_THICKNESS) {
        // -- Center area
        my = 1;
      } // Else: Bottom side scaler
      this.scalerMultiplier = mx !== 1 && my !== 1 ? Math.SQRT2 : 1.0;
      return my * 3 + mx + 1;
    } else if (this.aabbRotator.isPointInside(mouseX, mouseY)) {
      return 10;
    } else {
      return 0;
    }
  }
}
