// ShapeTools
// Transformation and conversion functions for shapes.

import { Vec2 } from "./vec2";

export class ShapeTools {
  // Generates path2D from list of vertices.
  public static ToPath2D(verts: Vec2[]): Path2D {
    if (verts.length === 0) {
      throw new Error("Shape.VerticesToPath2D() given empty vertex list.");
    }
    const path: Path2D = new Path2D();
    path.moveTo(verts[0].x, verts[0].y);
    for (const v of verts) {
      path.lineTo(v.x, v.y);
    }
    path.closePath();
    return path;
  }

  // Shape transformation
  public static Translate(verts: Vec2[], deltaxy: [number, number]): Vec2[] {
    const tverts: Vec2[] = [];
    for (const v of verts) {
      tverts.push(v.add(new Vec2(deltaxy[0], deltaxy[1])));
    }
    return tverts;
  }

  public static Rotate(verts: Vec2[], centerxy: [number, number], angleRadians: number): Vec2[] {
    const tverts: Vec2[] = [];
    for (const v of verts) {
      tverts.push(v.rotate(angleRadians, new Vec2(centerxy[0], centerxy[1])));
    }
    return tverts;
  }

  public static Scale(
    verts: Vec2[],
    centerxy: [number, number],
    scalexy: [number, number]
  ): Vec2[] {
    const tverts: Vec2[] = [];
    const center: Vec2 = new Vec2(centerxy[0], centerxy[1]);
    for (const v of verts) {
      tverts.push(
        v
          .sub(center)
          .scalexy(scalexy[0], scalexy[1])
          .add(center)
      );
    }
    return tverts;
  }

  // Shape creation
  public static Rect(width: number, height: number): Vec2[] {
    return [
      new Vec2(-width / 2, -height / 2),
      new Vec2(+width / 2, -height / 2),
      new Vec2(+width / 2, +height / 2),
      new Vec2(-width / 2, +height / 2)
    ];
  }

  public static Circle(radius: number, circleSegments: number = 32): Vec2[] {
    const v: Vec2[] = [];
    for (let r = 0.0; r < 2 * Math.PI; r += 2 * Math.PI / circleSegments) {
      v.push(new Vec2(Math.cos(r) * radius, Math.sin(r) * radius));
    }
    return v;
  }

  public static Triangle(baseWidth: number, height: number): Vec2[] {
    const v: Vec2[] = [];
    const x = -baseWidth / 2;
    const y = -height / 2;
    v.push(new Vec2(x + baseWidth / 2, y));
    v.push(new Vec2(x + baseWidth, y + height));
    v.push(new Vec2(x, y + height));
    return v;
  }

  public static Star(radius: number): Vec2[] {
    const v: Vec2[] = [];
    const innerRadius = 0.5 * radius;
    const points = 5.0;
    const angle = Math.PI * 2 / (points * 2);
    for (let i = points * 2 + 1; i > 0; --i) {
      const r = i % 2 === 1 ? radius : innerRadius;
      const omega = angle * i;
      const tx = r * Math.sin(omega);
      const ty = r * Math.cos(omega);
      v.push(new Vec2(tx, ty + 7)); // star slightly offset upwards
    }
    return v;
  }
}
