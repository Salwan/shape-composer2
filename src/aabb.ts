// AABB
// Simple bounding boxes

export class AABB {
  public static FromRect(x: number, y: number, width: number, height: number): AABB {
    const aabb: AABB = new AABB(x, x + width, y, y + height);
    return aabb;
  }

  public minX: number;
  public maxX: number;
  public minY: number;
  public maxY: number;

  // Constructor uses MAX and MIN Number values to allow easier AABB calculation
  public constructor(
    minX: number = Number.MAX_VALUE,
    maxX: number = Number.MIN_VALUE,
    minY: number = Number.MAX_VALUE,
    maxY: number = Number.MIN_VALUE
  ) {
    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;
  }

  public isPointInside(px: number, py: number): boolean {
    if (px < this.minX || px > this.maxX || py < this.minY || py > this.maxY) {
      return false;
    } else {
      return true;
    }
  }

  get width(): number {
    return Math.abs(this.maxX - this.minX);
  }

  get height(): number {
    return Math.abs(this.maxY - this.minY);
  }

  get center(): [number, number] {
    return [this.minX + this.width / 2, this.minY + this.height / 2];
  }
}
