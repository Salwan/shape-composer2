// AppEvents
// Classes for event management and dispatch.

import { Frame } from "./frame";

export const Events = {
  CREATE: "create", // Create new shape
  DELETE: "delete", // Delete selected shape
  NOSELECTION: "noselection", // Nothing is selected
  RESETSCENE: "reset", // Reset scene to default
  SELECTION: "selection", // Something is selected
  STORESCENE: "store" // Scene changed, store scene data for persistency
};

export interface IEvent {
  type: string;
  data: any;
}

export class AppEvents {
  private events: IEvent[];

  constructor() {
    this.events = [];
  }

  public broadcastEvent(type: string, data: any = null): void {
    this.events.push({ type, data });
  }

  // Receiver is any object that has a public onEvent(e:IEvent) method
  public runEventDispatcher(receivers: any[]): void {
    if (this.events.length > 0) {
      // Extract current events for iteration so any events added
      // during any onEvent call will be dispatched next frame:
      const evs: IEvent[] = this.events.splice(0, this.events.length);
      while (evs.length > 0) {
        const e: IEvent = evs.pop();
        for (const r of receivers) {
          r.onEvent(e);
        }
      }
    }
  }
}
