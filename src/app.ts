// App
// Entry point for application
// Manages app/browser level functionality

import { ComposerFrame } from "./composer";
import { AppEvents, Events, IEvent } from "./events";
import { Frame } from "./frame";
import { EShape } from "./shape";
import { Vec2 } from "./vec2";

export const PALETTE: string[] = ["#880000", "#CC44CC", "#00CC55", "#0000AA", "#EEEE77", "#DD8855"];

export interface IAppCanvas {
  canvas: HTMLCanvasElement;
  context: CanvasRenderingContext2D;
  collCanvas: HTMLCanvasElement;
  collContext: CanvasRenderingContext2D;
}

export class App {
  private composerFrame: ComposerFrame;
  private appCanvas: IAppCanvas;
  private appEvents: AppEvents;
  private deleteButton: HTMLButtonElement;

  // Mouse events
  private mouseDownPos: Vec2;
  private mouseDown: boolean;
  private mouseDrag: boolean;
  private mouseOverFrame: Frame;
  private mouseDragFrame: Frame;

  constructor(canvas: HTMLCanvasElement, context: CanvasRenderingContext2D) {
    // App Canvas
    this.appCanvas = { canvas, context } as IAppCanvas;

    // Create collision canvas
    this.appCanvas.collCanvas = document.createElement("canvas");
    this.appCanvas.collCanvas.width = this.appCanvas.canvas.width;
    this.appCanvas.collCanvas.height = this.appCanvas.canvas.height;
    this.appCanvas.collContext = this.appCanvas.collCanvas.getContext("2d");

    // Mouse events data init
    this.mouseDownPos = null;
    this.mouseDown = false;
    this.mouseDrag = false;
    this.mouseOverFrame = null;
    this.mouseDragFrame = null;
  }

  public start(): void {
    const ctx = this.appCanvas.context;
    const canvas = this.appCanvas.canvas;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    this.appEvents = new AppEvents();

    // Init Frames
    this.composerFrame = new ComposerFrame(this.appEvents, this.appCanvas, 0, canvas.width);

    // Init button events
    document.getElementById("reset").addEventListener("click", this.onButtonReset, false);
    document.getElementById("rect").addEventListener("click", this.onButtonRect, false);
    document.getElementById("circle").addEventListener("click", this.onButtonCircle, false);
    document.getElementById("triangle").addEventListener("click", this.onButtonTriangle, false);
    document.getElementById("star").addEventListener("click", this.onButtonStar, false);
    this.deleteButton = document.getElementById("delete") as HTMLButtonElement;
    this.deleteButton.addEventListener("click", this.onButtonDelete, false);

    // Init mouse events
    canvas.addEventListener("mousedown", this.onMouseDown, false);
    canvas.addEventListener("mouseup", this.onMouseUp, false);
    canvas.addEventListener("mousemove", this.onMouseMove, false);
  }

  public onFrame(): void {
    const collCtx = this.appCanvas.collContext;
    const collCanvas = this.appCanvas.collCanvas;
    // Run events dispatch
    this.appEvents.runEventDispatcher([this, this.composerFrame]);
    // Clear collision frame
    collCtx.clearRect(0, 0, collCanvas.width, collCanvas.height);
    this.composerFrame.draw();
  }

  public onEvent(event: IEvent): void {
    if (event.type === Events.SELECTION) {
      this.deleteButton.disabled = false;
    } else if (event.type === Events.NOSELECTION || event.type === Events.DELETE) {
      this.deleteButton.disabled = true;
    }
  }

  // Translate low level input events to higher level events:
  // - onMouseClick: indicates a down then up with no movement in-between
  // - onMouseOver: indicates mouse movement over frame
  // - onMouseLeave: indicates mouse moved out of the frame
  // Mouse drag applies to the same frame until drag ends (even if cursor left frame):
  // - onMouseDragBegin: indicates a button down and move action start
  // - onMouseDragUpdate: indicates a continued drag event
  // - onMouseDragEnd: indicates a button up after dragging
  public onMouseDown = (event: MouseEvent): void => {
    this.mouseDownPos = new Vec2(
      event.x - this.appCanvas.canvas.offsetLeft,
      event.y - this.appCanvas.canvas.offsetTop + window.pageYOffset
    );
    this.mouseDown = true;
  };

  public onMouseMove = (event: MouseEvent): void => {
    const pos = new Vec2(
      event.x - this.appCanvas.canvas.offsetLeft,
      event.y - this.appCanvas.canvas.offsetTop + window.pageYOffset
    );
    if (this.mouseDown) {
      // begin of drag
      this.mouseDragFrame = this.composerFrame;
      this.mouseDragFrame.onMouseDragBegin(this.mouseDownPos.x, this.mouseDownPos.y);
      this.mouseDrag = true;
      this.mouseDown = false;
    } else if (this.mouseDrag) {
      // drag update
      this.mouseDragFrame.onMouseDragUpdate(pos.x, pos.y);
    } else {
      // mouse over
      const overFrame = this.composerFrame;
      overFrame.onMouseOver(pos.x, pos.y);
      if (this.mouseOverFrame !== null && this.mouseOverFrame !== overFrame) {
        // Mouse moved out of a frame
        this.mouseOverFrame.onMouseLeave();
      }
      this.mouseOverFrame = overFrame;
    }
  };

  public onMouseUp = (event: MouseEvent): void => {
    const pos = new Vec2(
      event.x - this.appCanvas.canvas.offsetLeft,
      event.y - this.appCanvas.canvas.offsetTop + window.pageYOffset
    );
    if (this.mouseDrag) {
      // drag over
      this.mouseDragFrame.onMouseDragEnd(pos.x, pos.y);
      this.mouseDrag = false;
    } else if (this.mouseDown) {
      // click
      this.composerFrame.onMouseClick(pos.x, pos.y);
      this.mouseDown = false;
    }
  };

  // Button events
  public onButtonReset = (event: MouseEvent): void => {
    this.appEvents.broadcastEvent(Events.RESETSCENE);
  };

  public onButtonRect = (event: MouseEvent): void => {
    this.appEvents.broadcastEvent(Events.CREATE, EShape.rect);
  };

  public onButtonCircle = (event: MouseEvent): void => {
    this.appEvents.broadcastEvent(Events.CREATE, EShape.circle);
  };

  public onButtonTriangle = (event: MouseEvent): void => {
    this.appEvents.broadcastEvent(Events.CREATE, EShape.triangle);
  };

  public onButtonStar = (event: MouseEvent): void => {
    this.appEvents.broadcastEvent(Events.CREATE, EShape.star);
  };

  public onButtonDelete = (event: MouseEvent): void => {
    this.appEvents.broadcastEvent(Events.DELETE);
  };
}
