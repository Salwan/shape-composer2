// CanvasTools
// Shortcuts to some frequently used canvas routines.

import { Rect } from "./rect";

export abstract class CanvasTools {
  public static DrawLine(
    ctx: any,
    color: string,
    start: [number, number],
    delta: [number, number]
  ): void {
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.moveTo(start[0], start[1]);
    ctx.lineTo(start[0] + delta[0], start[1] + delta[1]);
    ctx.stroke();
  }

  public static DrawPath(ctx: any, path: Path2D, fillColor: string, strokeColor: string): void {
    // Button background
    ctx.beginPath();
    ctx.fillStyle = fillColor;
    ctx.fill(path);
    ctx.strokeStyle = strokeColor;
    ctx.stroke(path);
  }

  public static DrawCircle(
    ctx: any,
    x: number,
    y: number,
    radius: number,
    fillColor: string,
    strokeColor: string,
    lineWidth: number = 1.0
  ) {
    ctx.save();
    ctx.beginPath();
    ctx.fillColor = fillColor;
    ctx.strokeColor = strokeColor;
    ctx.arc(x, y, radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();
    ctx.restore();
  }

  public static FillPath(ctx: any, path: Path2D, fillColor: string) {
    ctx.fillStyle = fillColor;
    ctx.beginPath();
    ctx.fill(path);
  }

  public static StrokePath(ctx: any, path: Path2D, strokeColor: string, lineWidth: number = 1.0) {
    ctx.save();
    ctx.strokeStyle = strokeColor;
    ctx.lineWidth = lineWidth;
    ctx.beginPath();
    ctx.stroke(path);
    ctx.restore();
  }

  public static StrokeRect(ctx: any, rect: Rect, strokeColor: string, lineWidth: number = 1.0) {
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = strokeColor;
    ctx.lineWidth = lineWidth;
    ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
    ctx.restore();
  }
}
