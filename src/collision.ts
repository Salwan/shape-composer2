// Collision
// Collision operations for shapes.

import * as assert from "assert";

const ENTITY_COLOR_MULTIPLIER: number = 0x20;

export abstract class Collision {
  // Converts entity id to html color as a string: #NNNNNN
  public static IdToColor(id: number): string {
    let color = (id * ENTITY_COLOR_MULTIPLIER).toString(16);
    assert(color.length <= 6);
    while (color.length < 6) {
      color = "0" + color;
    }
    return "#" + color;
  }

  // Converts html color (#NNNNNN) to entity id
  public static ColorToId(color: string): number {
    return (parseInt(color.substr(1), 16) & 0xffffff) / ENTITY_COLOR_MULTIPLIER;
  }

  // Data given as: [R,G,B,A]
  public static RGBToId(data: Uint8ClampedArray): number {
    return ((data[0] << 16) + (data[1] << 8) + data[2]) / ENTITY_COLOR_MULTIPLIER;
  }
}
