// MouseCursor
// Provides functions for setting mouse cursor.

import * as assert from "assert";
import { IAppCanvas } from "./app";

const cursors: string[] = [
  "auto",
  "nw-resize",
  "n-resize",
  "ne-resize",
  "w-resize",
  "move",
  "e-resize",
  "sw-resize",
  "s-resize",
  "se-resize",
  "col-resize"
];

export abstract class MouseCursor {
  public static ResetMouseCursor(appCanvas: IAppCanvas): void {
    appCanvas.canvas.style.cursor = "auto";
  }

  // Set cursor to movement cross
  public static SetMouseCursorToCross(appCanvas: IAppCanvas): void {
    appCanvas.canvas.style.cursor = "move";
  }

  // Set cursor to directional scaling or movement cross:
  //   8  1  2   NW N NE
  //   7  9  3 = W  +  E
  //   6  5  4   SW S SE
  public static SetMouseCursorTransform(appCanvas: IAppCanvas, mode: number): void {
    appCanvas.canvas.style.cursor = cursors[mode];
  }

  public static SetMouseCursorRotator(appCanvas: IAppCanvas): void {
    appCanvas.canvas.style.cursor = "col-resize";
  }
}
