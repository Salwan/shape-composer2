import * as mocha from "mocha";
import * as chai from "chai";
import { ShapeTools } from "../src/shape-tools";
import { Vec2 } from "../src/vec2";

// Transformation Test Package
mocha.describe('Transformation', function() {
    // Test Unit 1:
    mocha.describe('Transform Math', () => {
        let v0:Vec2, v1:Vec2, v2:Vec2, v3:Vec2, vo:Vec2;
        let sx:number, sy:number;
        let dist:number, angle:number;
        
        mocha.beforeEach(() => {
            v0 = new Vec2(1, 0);
            v1 = new Vec2(10, 10);
            v2 = new Vec2(5, 5);
            v3 = new Vec2(-1, 0);
            vo = new Vec2();
            sx = 1.33;
            sy = 1.33;
            angle = 50 * Math.PI / 180.0;
        });

        // Math tests
        mocha.it('should measure distance of unit vector', () => {
            const dist = v0.distance();
            chai.expect(dist).approximately(1.0, 0.01);
        });
        mocha.it('should measure distance of vector', () => {
            dist = v1.distance();
            chai.expect(dist).approximately(14.1421, 0.01);
        });
        mocha.it('should scale a vector by x and y', () => {
            v1 = v1.scalexy(sx, sy);
            chai.expect(v1.x).approximately(10 * sx, 0.01);
            chai.expect(v1.y).approximately(10 * sy, 0.01);
        });
        mocha.it('should normalize a vector correctly', () => {
            v2 = v2.normal();
            chai.expect(v2.distance()).approximately(1.0, 0.01);
        });
        mocha.it('should calculate direction', () => {
            chai.expect(v3.direction()).approximately(180.0 * Math.PI / 180, 0.01);
        });
        mocha.it('should add two vectors', () => {
            v1 = v1.add(v2);
            chai.expect(v1.distance()).approximately(Math.sqrt((15 * 15) + (15 * 15)), 0.01);
        });
        it('should rotate vector around a point', () => {
            v3 = v3.rotate(-90 * Math.PI / 180.0, new Vec2(0, 0));
            chai.expect(v3.direction()).approximately(90.0 * Math.PI / 180.0, 0.01);
        });
    });

    // Test Unit 2
    mocha.describe('Shape Transformation', () => {
        // Test triangle
        let triangle:Vec2[];
        // Test triangle rotated by 90 degrees right
        let triangleRotated:Vec2[];
        // Test triangle scaled uniformly by 2
        let triangleScaled:Vec2[];
        // Test triangle translated by -2, -2 (centered on origin)
        let triangleTranslated:Vec2[];
        // Combination: moved to center -> rotated 180 degrees -> scaled to 0.5x
        let triangleTransformed:Vec2[];

        // Convenience test function
        let comparatorTest:Function = (vertsTest:Vec2[], vertsExpected:Vec2[], message:string) => {
            chai.expect(vertsTest.length).to.equal(vertsExpected.length, message + " have different number of elements");
            for(let i = 0; i < vertsTest.length; ++i) {
                chai.expect(vertsTest[i].x).to.be.approximately(vertsExpected[i].x, 0.1, message + ` vertex[${i}].x is incorrect`);
                chai.expect(vertsTest[i].y).to.be.approximately(vertsExpected[i].y, 0.1, message + ` vertex[${i}].y is incorrect`);
            }
        }

        mocha.beforeEach(() => {
            triangle = [
                new Vec2(2.0, 0.0),
                new Vec2(4.0, 4.0),
                new Vec2(0.0, 4.0)
            ];
            // Transled to center on origin
            triangleTranslated = [
                new Vec2(0.0, -2.0),
                new Vec2(2.0, 2.0),
                new Vec2(-2.0, 2.0)
            ];
            // Rotated by 90 degrees right
            triangleRotated = [
                new Vec2(4.0, 2.0),
                new Vec2(0.0, 4.0),
                new Vec2(0.0, 0.0)
            ];
            // Scaled by 2x from origin point
            triangleScaled = [
                new Vec2(4.0, 0.0),
                new Vec2(8.0, 8.0),
                new Vec2(0.0, 8.0)
            ];
            // Combination: moved to center -> rotated 180 degrees -> scaled to 0.5x
            triangleTransformed = [
                new Vec2(0.0, 1.0),
                new Vec2(-1.0, -1.0),
                new Vec2(1.0, -1.0) 
            ];
        });
        
        // Transform triangle
        mocha.it('should translate triangle to center on origin', () => {
            const t:Vec2[] = ShapeTools.Translate(triangle, [-2, -2]);
            comparatorTest(t, triangleTranslated, "Triangle Translation - ");
        });
        mocha.it('should rotate triangle by 90 degrees', () => {
            const t:Vec2[] = ShapeTools.Rotate(triangle, [2, 2], 90 * Math.PI / 180);
            comparatorTest(t, triangleRotated, "Triangle Rotation - ");
        });
        mocha.it('should scale triangle 2x from origin', () => {
            const t:Vec2[] = ShapeTools.Scale(triangle, [0, 0], [2, 2]);
            comparatorTest(t, triangleScaled, "Triangle Scaling - ");
        });
        mocha.it('should combine translation (-2,-2), rotation (180degs), scaling(0.5x) correctly', () => {
            let t:Vec2[] = ShapeTools.Translate(triangle, [-2, -2]);
            t = ShapeTools.Rotate(t, [0, 0], Math.PI);
            t = ShapeTools.Scale(t, [0, 0], [0.5, 0.5]);
            comparatorTest(t, triangleTransformed, "Triangle Translate/Rotate/Scale - ");
        });
    });
});