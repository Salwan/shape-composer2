# Shape-Verse 2 (ShapeComposer2 internally)

Project uses the following solutions:

- Typescript
- Gulp
- TSLint
- Prettier
- Mocha/Chai
- Browserify and Browser-sync

The development strategy was to implement the application directly using a simple architecture.

I was not able to get one linting rule enforced: function line limit. As it is not available in TSLint.

## Running App

Live test here: http://zenithsal.com/other/shapeverse21k/

Or run `gulp serve`.

Or open `release/index.html` for local test.

## Running unit tests

Implemented math tests and shape transformation tests as required, run `gulp test` to execute unit tests via [Mocha](https://mochajs.org/).

## Development server

Run `gulp serve` for a dev server. Navigate to `http://localhost:3000/`.

## Build

Run `gulp serve`, project build will be in `/dist` folder.
