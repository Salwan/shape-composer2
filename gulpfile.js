var gulp = require("gulp");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var watchify = require("watchify");
var tsify = require("tsify");
var tslint = require("gulp-tslint");
var prettier = require("gulp-prettier-plugin");
var browserSync = require("browser-sync").create();
var fancylog = require("fancy-log");
var mocha = require("gulp-mocha");
var paths = {
    pages: 'src/*.html',
    code: 'src/**/*.ts',
    testCode: [
      'test/**/*.ts',
      'src/vec2.ts',
      'src/shape-tools.ts'
    ],
    main: 'src/main.ts',
    index: 'src/index.html',
    build: 'dist',
    tsfiles: [
        'src/main.ts',
        'src/app.ts',
        'src/frame.ts',
        'src/composer.ts',
        'src/shape.ts',
        'src/shape-tools.ts',
        'src/vec2.ts',
        'src/aabb.ts',
        'src/events.ts',
        'src/collision.ts',
        'src/selection-box.ts',
        'src/mouse-cursor.ts',
        'src/persistence.ts',
        'src/canvas-tools.ts',
        'src/rect.ts'
    ]
};

gulp.task("copy-html", function () {
    return gulp.src(paths.pages)
        .pipe(gulp.dest(paths.build));
});

gulp.task("lint", function() {
    return gulp.src([paths.code])
        .pipe(tslint({
            configuration: "tslint.json"
        }))
        .pipe(tslint.report({
            emitError: false,
            reportLimit: 16
        }));
});

gulp.task("pretify", function() {
    return gulp.src([paths.code])
        .pipe(prettier({ printWidth: 100 }, {filter: true}))
        .pipe(gulp.dest(file => file.base))
});

gulp.task("browser-sync", function() {
    browserSync.init({
        server: {
            baseDir: paths.build
        }
    });
    gulp.watch(paths.pages).on("change", browserSync.reload);
});

gulp.task('test', function() {
  return gulp.src('test/*.spec.ts')
    .pipe(mocha({
      reporter: 'spec',
      require: ['ts-node/register']
    }))
})

var watchedBrowserify = watchify(browserify({
    basedir: '.',
    debug: true,
    entries: paths.tsfiles,
    cache: {},
    packageCache: {}
}).plugin(tsify));

function bundle() {
    return watchedBrowserify
        .bundle()
        .on('error', function(err){
            // print the error (can replace with gulp-util)
            console.log(err.message);
            // end this stream
            this.emit('end');
          })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest(paths.build))
        .pipe(browserSync.reload({ stream: true }));
}

gulp.task("serve", ["copy-html", "browser-sync"], bundle);
gulp.task("watch", function() {
    gulp.watch(paths.code, ["pretify", "lint", "copy-html"]);
    gulp.watch([paths.code, paths.pages], ["copy-html"]);
    gulp.watch(paths.testCode, ["test"]);
});

watchedBrowserify.on("update", bundle);
watchedBrowserify.on("log", function(msg) {
    console.log(msg);
});
